import 'package:flutter/material.dart';
import 'package:xkcd/src/rust/api/simple.dart';
import 'package:xkcd/src/rust/frb_generated.dart';

import 'package:http/http.dart' as http;
import 'package:share_plus/share_plus.dart';
import 'package:url_launcher/url_launcher.dart';

Future<void> main() async {
  await RustLib.init();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'XKCD',
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.grey,
          leading: const Padding(
              padding: EdgeInsets.only(left: 10.0),
              child:
                  Image(image: NetworkImage('https://xkcd.com/s/0b7742.png'))),
          title: const Text(
              'A webcomic of romance, sarcasm,\nmath and language.',
              style: TextStyle(
                  fontSize: 16,
                  fontStyle: FontStyle.italic,
                  fontWeight: FontWeight.w500,
                  color: Color.fromARGB(255, 51, 51, 51))),
        ),
        backgroundColor: const Color.fromARGB(236, 239, 243, 248),
        body: const Center(
          child: ComicWidget(),
        ),
      ),
    );
  }
}

class ComicWidget extends StatefulWidget {
  const ComicWidget({super.key});

  @override
  State<ComicWidget> createState() => _ComicWidgetState();
}

class _ComicWidgetState extends State<ComicWidget> {
  int _id = 0;
  String _title = "";
  String _url = "about:blank";
  String _alt = "";
  String _published = "";

  _launchURL() async {
    final Uri url = Uri.parse('https://xkcd.com/$_id');
    if (!await launchUrl(url)) {
      throw Exception('Could not launch $_url');
    }
  }

  @override
  Widget build(BuildContext context) {
    void getReady() async {
      final comic = await firstRender();
      setState(() {
        _id = comic.id;
        _title = comic.title;
        _url = comic.img;
        _alt = comic.alt;
        _published = comic.published;
      });
    }

    if (_id == 0) {
      getReady();
    }

    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
      ButtonBar(alignment: MainAxisAlignment.spaceEvenly, children: [
        ElevatedButton(
            style: const ButtonStyle(
              backgroundColor: WidgetStatePropertyAll(Colors.grey),
            ),
            onPressed: _launchURL,
            child: const Text("Official site",
                style: TextStyle(
                    fontSize: 14, color: Color.fromARGB(255, 255, 255, 255)))),
        shareButton('Share this comic', () async {
          final url = Uri.parse(_url);
          final response = await http.get(url);
          Share.shareXFiles([
            XFile.fromData(response.bodyBytes,
                name: _title, mimeType: 'image/png'),
          ], subject: _title);
        })
      ]),
      Center(
          child: Text("$_id - $_title",
              style: const TextStyle(
                  height: 2,
                  fontSize: 18,
                  color: Color.fromARGB(255, 51, 51, 51),
                  fontWeight: FontWeight.bold))),
      Padding(
          padding: const EdgeInsets.only(bottom: 10.0),
          child: Center(
              child: Text(_published,
                  style: const TextStyle(
                      height: 2,
                      fontSize: 14,
                      color: Color.fromARGB(255, 51, 51, 51),
                      fontWeight: FontWeight.w500)))),
      Expanded(
          child: GestureDetector(
              onHorizontalDragEnd: (DragEndDetails details) async {
                if (details.velocity.pixelsPerSecond.dx > 0) {
                  final comic = await previous(id: _id);
                  setState(() {
                    _id = comic.id;
                    _title = comic.title;
                    _url = comic.img;
                    _alt = comic.alt;
                    _published = comic.published;
                  });
                } else if (details.velocity.pixelsPerSecond.dx < 0) {
                  final comic = await next(id: _id);
                  setState(() {
                    _id = comic.id;
                    _title = comic.title;
                    _url = comic.img;
                    _alt = comic.alt;
                    _published = comic.published;
                  });
                }
              },
              onVerticalDragEnd: (DragEndDetails details) async {
                if (details.velocity.pixelsPerSecond.dy > 0) {
                  final comic = await random();
                  setState(() {
                    _id = comic.id;
                    _title = comic.title;
                    _url = comic.img;
                    _alt = comic.alt;
                    _published = comic.published;
                  });
                } else if (details.velocity.pixelsPerSecond.dy < 0) {
                  final comic = await latest();
                  setState(() {
                    _id = comic.id;
                    _title = comic.title;
                    _url = comic.img;
                    _alt = comic.alt;
                    _published = comic.published;
                  });
                }
              },
              child: Semantics(
                  hint:
                      "Swipe left or right for previous and next comic, up for latest, down for random",
                  image: true,
                  label: _alt,
                  child: Image.network(_url)))),
    ]);
  }
}

shareButton(String title, Function()? onPressed) {
  return ElevatedButton(
    style: ButtonStyle(
      backgroundColor: WidgetStateProperty.all(Colors.grey),
    ),
    onPressed: onPressed,
    child: Text(title,
        style: const TextStyle(
            fontSize: 14, color: Color.fromARGB(255, 255, 255, 255))),
  );
}
