use std::sync::OnceLock;

use super::utils::Comic;

// LAST is used when the png cannot be displayed to jump forth to the next one on the following swipe
static mut LAST: u16 = 0;
// LATEST is the id of latest comic published when the application is launched
static LATEST: OnceLock<u16> = OnceLock::new();

#[flutter_rust_bridge::frb(init)]
pub fn init_app() {
    flutter_rust_bridge::setup_default_user_utils();
}

pub async fn first_render() -> Comic {
    let comic = Comic::get(None)
        .await
        .expect("Reqwest just CAN'T fail, right?");

    _ = LATEST.set(comic.id);

    comic
}

pub async fn latest() -> Comic {
    Comic::get(None)
        .await
        .expect("Reqwest just CAN'T fail, right?")
}

pub async fn random() -> Comic {
    Comic::random(*LATEST.get().expect("LATEST should be set"))
        .await
        .expect("Reqwest just CAN'T fail, right?")
}

pub async fn previous(mut _id: u16) -> Comic {
    unsafe {
        if _id == LAST {
            _id -= 1;
        } else {
            LAST = _id;
        }
    }

    if _id == 1 {
        _id = *LATEST.get().unwrap() + 1;
    }

    Comic::get(Some(_id - 1))
        .await
        .expect("Reqwest just CAN'T fail, right?")
}

pub async fn next(mut _id: u16) -> Comic {
    unsafe {
        if _id == LAST {
            _id += 1;
        } else {
            LAST = _id;
        }
    }

    if _id == *LATEST.get().unwrap() {
        _id = 0;
    }

    Comic::get(Some(_id + 1))
        .await
        .expect("Reqwest just CAN'T fail, right?")
}
