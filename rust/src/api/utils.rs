use rand::{thread_rng, Rng};
use serde::Deserialize;

#[derive(Deserialize)]
pub struct ComicUnfiltered {
    month: String,
    num: u16,
    link: String,
    year: String,
    news: String,
    safe_title: String,
    transcript: String,
    alt: String,
    img: String,
    title: String,
    day: String,
}

impl From<ComicUnfiltered> for Comic {
    fn from(unfiltered: ComicUnfiltered) -> Self {
        let ComicUnfiltered {
            month,
            num,
            year,
            alt,
            img,
            title,
            day,
            ..
        } = unfiltered;

        let month = match month
            .parse::<u8>()
            .expect("Month is supposed to be a number")
        {
            1 => "Jan",
            2 => "Feb",
            3 => "Mar",
            4 => "Apr",
            5 => "May",
            6 => "Jun",
            7 => "Jul",
            8 => "Aug",
            9 => "Sep",
            10 => "Oct",
            11 => "Nov",
            12 => "Dec",
            _ => panic!("And a merry X-m... wait... what month is it again?"),
        };
        let day = match day.parse::<u8>().expect("Day is supposed to be number") {
            x if x % 10 == 1 && x != 11 => format!("{x}st"),
            y if y % 10 == 2 && y != 12 => format!("{y}nd"),
            z if z % 10 == 3 && z != 13 => format!("{z}rd"),
            _ => format!("{day}th"),
        };
        let published = format!("{} {}, {}", month, day, year);

        Self {
            id: num,
            title,
            published,
            img,
            alt,
        }
    }
}

pub struct Comic {
    pub id: u16,
    pub title: String,
    pub published: String,
    pub img: String,
    pub alt: String,
}

impl Comic {
    pub async fn get(id: Option<u16>) -> Result<Self, reqwest::Error> {
        let _id = if let Some(num) = id {
            format!("/{num}")
        } else {
            String::new()
        };

        reqwest::get(&format!("https://xkcd.com{_id}/info.0.json"))
            .await?
            .json::<ComicUnfiltered>()
            .await
            .map(|comic| Comic::from(comic))
    }

    pub async fn random(max: u16) -> Result<Self, reqwest::Error> {
        reqwest::get(&format!(
            "https://xkcd.com/{}/info.0.json",
            thread_rng().gen_range(0..max)
        ))
        .await?
        .json::<ComicUnfiltered>()
        .await
        .map(|comic| Comic::from(comic))
    }
}
